import React, {Component} from 'react';

function RenderButton(props) {
    return(
        <button value={props.value} onClick={props.onClick}>{props.name}</button>
    );
}

class CharacterSelect extends Component {
    render() {
        const names = this.props.name;
        const selectNames = names.map((e, i) => {
            return(
                <RenderButton key={i} name={e} value={e} onClick={() => this.props.onClick(i)} />
            );
        });

        return (
            <>{selectNames}</>
        );
    }
}

class Header extends Component {
    render() {
        const name = this.props.names;
        return (
            <CharacterSelect name={name} onClick={(i) => this.props.onClick(i)} />
        );
    }
}

export default Header;