import React from "react";

function Attribute(props) {
    return(
        <>
            <div className="uppercase">{props.title}:</div>
            <div className="grid-end pe-3">{props.base}</div>
            <div>(modifier:</div>
            <div className="grid-end">{props.modifier > 0 ? '+' + props.modifier : props.modifier})</div>
        </>
    );
}

function Skills(props) {
    return (
        <>
            <div className="grid-center">
                <div className={"circle" + (props.selected ? " bg-smoke" : "")}></div>
            </div>
            <div className="capitalize">{props.title}: </div>
            <div className="grid-end">{props.value}</div>
        </>
    );
}

function Throws(props) {
    return (
        <>
            <div className="grid-center">
                <div className={"circle" + (props.selected ? " bg-smoke" : "")}></div>
            </div>
            <div className="uppercase">{props.title}</div>
            <div className="grid-end">{props.value}</div>
        </>
    );
}

export const Attributes = (data) => {
    const base = Object.keys(data.attributes.base).map(function(key) {
        let modifier = Math.floor((data.attributes.base[key] - 10)/2);
        return (
            <Attribute key={key} modifier={modifier} base={data.attributes.base[key]} title={key} />
        );
    });

    function Proficiency(props) {
        return (
            <div className="col">
                <strong>Proficiency</strong>: {props.proficiency}
            </div>
        );
    }

    const skills = Object.keys(data.attributes.skills).map((key) => (
        <Skills key={key} selected={data.attributes.skills[key].selected} value={data.attributes.skills[key].value} title={key} />
    ));

    const savingThrows = Object.keys(data.attributes.savingThrows).map((key) => (
        <Throws key={key} selected={data.attributes.savingThrows[key].selected} title={key} value={data.attributes.savingThrows[key].value} />
    ));

    function Other(props) {
        return (
            <>
                <div className="col-12">
                    {props.languages.join(', ')}
                </div>
                <div className="col-12">
                    {props.proficiencies.join(', ')}
                </div>
            </>
        );
    }

    return (
        <div className="row">
            <div className="col-6 col-md-12 col-lg-6 mb-2">
                <h3 className="h4">Skills</h3>
                <div className="grid-3">
                    {skills}
                </div>
            </div>
            <div className="col-6 col-md-12 col-lg-6">
                <h3 className="h4">Attributes</h3>
                <div className="grid-4 mb-2">
                    {base}
                </div>

                <div className="row mb-2">
                    <Proficiency proficiency={data.attributes.proficiency} />
                </div>

                <h3 className="h4">Saving Throws</h3>
                <div className="grid-3 mb-2">
                    {savingThrows}
                </div>

                <h3 className="h4">Other</h3>
                <div className="row mb-2">
                    <Other languages={data.attributes.other.languages} proficiencies={data.attributes.other.proficiencies} />
                </div>

            </div>
        </div>
    );
}