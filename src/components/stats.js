import React, {Component} from 'react';

function Circles(props) {
    const s = props.saves;
    const i = props.index;

    return (
        <div className="col">
            <div className={"circle" + (s > i ? ' bg-smoke' : '')}></div>
        </div>
    );
}

class Stats extends Component {

    render() {
        const stats = this.props.stats;
        const saves = new Array(stats.deathSuccess.max).fill(stats.deathSuccess.current).map( (e, i) =>
            <Circles key={i} saves={e} index={i} />
        );
        const failures = new Array(stats.deathFalures.max).fill(stats.deathFalures.current).map( (e, i) =>
            <Circles key={i} saves={e} index={i} />
        );

        return (
            <div className='row mb-2'>
                <div className='col-4 col-xl-3'>
                    <div className='text-center capitalize mb-2'>
                        Armor class
                        <div className='text-large'>{stats.armor}</div>
                    </div>
                    <div className='text-center mb-2'>
                        Initiative
                        <div className='text-large'>{stats.initiative}</div>
                    </div>
                    <div className='text-center'>
                        Speed
                        <div className='text-large'>{stats.speed}</div>
                    </div>
                </div>
                <div className='col-4 col-xl-3'>
                    <div className='capitalize text-center mb-2'>
                        hit points
                        <div className='text-large'>{stats.hp} / {stats.maxHp}</div>
                    </div>
                    <div className='capitalize text-center'>
                        Temp hp
                        <div className='text-large'>{stats.tempHp}</div>
                    </div>
                </div>
                <div className='col-4 col-xl-3'>
                    <div className='text-center'>Hit Dice: 
                        <div className='text-large'>{stats.hitDice} d{stats.hitTotal}</div>
                    </div>
                </div>
                <div className='col-12 col-xl-3'>
                    <div className='row'>
                        <div className='col-6 col-xl-12'>
                            <div className='col-12'>Saves</div>
                            <div className="row row-cols-auto mb-3 pt-1">
                                {saves}
                            </div>
                        </div>
                        <div className='col-6 col-xl-12'>
                            <div className='col-12'>Failures</div>
                            <div className="row row-cols-auto pt-1">
                                {failures}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Stats;