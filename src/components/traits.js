import React, {Component} from 'react';

function Trait(props) {
    return (
        <div>{props.type} - {props.description}</div>
    );
}

class Traits extends Component {
    render() {
        const traits = this.props.traits;
        const personality = traits.personality.map( (e, i) =>
            <Trait key={i} type={e.type} description={e.description} />
        );
        const ideals = traits.ideals.map( (e, i) =>
            <Trait key={i} type={e.type} description={e.description} />
        );
        const bonds = traits.bonds.map( (e, i) =>
            <Trait key={i} type={e.type} description={e.description} />
        );
        const flaws = traits.flaws.map( (e, i) =>
            <Trait key={i} type={e.type} description={e.description} />
        );

        return (
            <div className='row'>
                <h3 className="h4">Traits</h3>
                <div className='col-12 col-md-6 col-lg-3 mb-3'>
                    <h4 className='h5'>Personailty traits:</h4>
                    {personality}
                </div>
                <div className='col-12 col-md-6 col-lg-3 mb-3'>
                    <h4 className='h5'>Ideals:</h4>
                    {ideals}
                </div>
                <div className='col-12 col-md-6 col-lg-3 mb-3'>
                    <h4 className='h5'>Bonds:</h4>
                    {bonds}
                </div>
                <div className='col-12 col-md-6 col-lg-3 mb-3'>
                    <h4 className='h5'>Flaws:</h4>
                    {flaws}
                </div>
            </div>
        );
    };
}

export default Traits;