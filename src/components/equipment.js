import React, {Component} from 'react';

function Items(props) {
    const item = props.item;

    return (
        <>
            <div className="">{item.quantity}</div>
            <div className="grid-justify-stretch">{item.name} {!!item.notes && <span className="red">*</span>}</div>
            <div className="grid-end">{typeof(item.weight) === 'number' ? item.weight * item.quantity : ''}</div>
        </>
    )
}

class Equipment extends Component {
    render() {
        const inventory = this.props.inventory.items;
        const items = inventory.map( (item, index) =>
            <Items key={index} item={item} index={index} />
        );
        const footNotes = inventory.filter(note => !!note.notes).map( (note, index) =>
            <p key={index}>{note.name}<span className="red">*</span>: {note.notes}</p>
        );
        const weight = inventory.filter((e) => e.weight > 0).reduce((p,c) => p + (c.weight * c.quantity), 0);
        //const currency = this.props.currency;

        return (
            <div className='row'>
                <h3 className="h4">Inventory</h3>
                <div className='col-6 col-md-12 col-lg-6'> 
                    <div className="grid-3 grid-block mb-3">
                        {items}
                        <div className="grid-span-2">Total Weight:</div>
                        <div className="grid-end">{weight}</div>
                    </div>
                </div>
                <div className='col-6 col-md-12 col-lg-6'>{footNotes}</div>
            </div>
        );
    }
}

export default Equipment;