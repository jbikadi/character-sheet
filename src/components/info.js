import React from "react";

export const Info = (c) => {
    const info = c.character;

    return (
        <div className="row">
            <div className="col-12 col-md-6">
                <div className="row">
                    <div className="col-6">
                        <div className="row mb-2">
                            <div className="col-auto">Name:</div>
                            <div className="col"><div className="underline">{info.name}</div></div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="row mb-2">
                            <div className="col-auto">Class:</div>
                            <div className="col"><div className="underline">{info.class}</div></div>
                        </div>
                    </div>
                    <div className="col col-sm-6">
                        <div className="row mb-2">
                            <div className="col-auto">Level:</div>
                            <div className="col"><div className="underline">{info.level}</div></div>
                        </div>
                    </div>
                    <div className="col col-sm-6">
                        <div className="row mb-2">
                            <div className="col-auto">Experience:</div>
                            <div className="col"><div className="underline">{info.xp}</div></div>
                        </div>
                    </div>
                    <div className="col col-sm-6">
                        <div className="row mb-2">
                            <div className="col-auto">Race:</div>
                            <div className="col"><div className="underline">{info.race}</div></div>
                        </div>
                    </div>
                    <div className="col col-sm-6">
                        <div className="row mb-2">
                            <div className="col-auto">Alignment:</div>
                            <div className="col"><div className="underline">{info.alignment}</div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-12 col-md-6">
                <div className="row">
                    <div className="col-12">
                        <span className="float-left me-3">Background:</span> {info.background}
                    </div>
                </div>
            </div>
        </div>
    );
}