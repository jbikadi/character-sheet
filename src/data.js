export const characters = [
    {
        info: {
            name: 'Bearmourn',
            class: 'Fighter',
            level: 1,
            background: 'Soldier. A half orc raised with an orc tribe in the spine of the world with in the kingdom of many arrows.',
            race: 'Half Orc',
            alignment: 'Evil',
            xp: 10
        },
        attributes: {
            base: {
                str: 16,
                dex: 11,
                con: 13,
                int: 8,
                wis: 9,
                chr: 10
            },
            proficiency: 2,
            savingThrows: {
                str: {value: 5, selected: true},
                dex: {value: -1, selected: null},
                con: {value: 4, selected: true},
                int: {value: 0, selected: null},
                wis: {value: 1, selected: null},
                chr: {value: 2, selected: null}
            },
            skills: {
                acrobatics: {value:-1, selected: true},
                animal: {value: 1, selected: null},
                arcana: {value: 0, selected: null},
                athletics: {value: 5, selected: true},
                deception: {value: 2, selected: null},
                history: {value: 2, selected: null},
                insight: {value: 1, selected: null},
                intimidation: {value: 2, selected: true},
                investigation: {value: 0, selected: null},
                medicine: {value: 1, selected: null},
                nature: {value: 0, selected: null},
                perception: {value: 3, selected: null},
                performance: {value: 2, selected: null},
                persuasion: {value: 4, selected: null},
                religion: {value: 0, selected: null},
                slight: {value: -1, selected: null},
                stealth: {value: -2, selected: null},
                survival: {value: 1, selected: null}
            },
            wisdom: 13,
            other: {
                proficiencies: ['All armor','shields', 'simple weapons', 'martial weapons'],
                languages: ['Common', 'Orcish', 'Goblin']
            }
        },
        stats: {
            armor: 18,
            initiative: 0,
            speed: 30,
            hp: 80,
            maxHp: 81,
            tempHp: 0,
            hitDice: 4,
            hitTotal: 8,
            deathSuccess: {
                current: 0,
                max: 3
            },
            deathFalures: {
                current: 2,
                max: 3
            }
        },
        attacks: [
            {
                name: "Greataxe",
                attBonus: 5,
                damage: "1d12 + 3",
                type: "slashing",
                notes: ""
            },
            {
                name: "Javelin",
                attBonus: 5,
                damage: "1d6 + 3",
                type: "piercing",
                notes: "You can throw a javelin 30 feet, or up to 120 feet with disadvantage on the attack roll."
            }
        ],
        equipment: {
            items: [
                {
                    name: "Chain mail",
                    quantity: 1,
                    weight: 35,
                    notes: "While wearing this armor, you have a disadvantage on Dex (stealth) checks."
                },{
                    name: "Great Axe",
                    quantity: 1,
                    weight: 12,
                    notes: null
                },{
                    name: "Javelin",
                    quantity: 3,
                    weight: 2,
                    notes: ""
                },{
                    name: "Backpack",
                    quantity: 1,
                    weight: 1,
                    notes: "Yes!"
                },{
                    name: "Blanket",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "Tinderbox",
                    quantity: 1,
                    weight: 0.1,
                    notes: ""
                },{
                    name: "Days of rations",
                    quantity: 2,
                    weight: 1,
                    notes: ""
                },{
                    name: "waterskin",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "set of fine clothes",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "signet ring",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "scroll of pedigree",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                }
            ],
            currency: {
                cp: null,
                sp: null,
                ep: null,
                gp: 24,
                pp: null
            }
        },
        traits: {
            personality: [
                {
                    type: "Danger",
                    description: "Danger exists"
                },{
                    type: "Torment",
                    description: "My curse etc"
                }
            ],
            ideals: [
                {
                    type: "Monster",
                    description: "Monster will destroy everyhing like the monster i am (evil)"
                }
            ],
            bonds: [
                {
                    type: "Rival",
                    description: "a monster of habbit"
                },{
                    type: "Gruumsh One-eye",
                    description: "a greater god of storms and war, and of orcs. He is the leader of the orc deities."
                }
            ],
            flaws: [
                {
                    type: "Rituals",
                    description: "a monster of habbit"
                }
            ]
        }
    },{
        info: {
            name: 'Dralxunthuur Diash',
            class: 'Barbarian',
            level: 2,
            background: 'Soldier',
            race: 'Dragonborn',
            alignment: 'Chaotic Neutral',
            xp: 100
        },
        attributes: {
            base: {
                str: 18,
                dex: 11,
                con: 14,
                int: 8,
                wis: 9,
                chr: 9
            },
            proficiency: 2,
            savingThrows: {
                str: {value: 6, selected: true},
                dex: {value: -1, selected: null},
                con: {value: 4, selected: true},
                int: {value: 0, selected: null},
                wis: {value: 1, selected: null},
                chr: {value: 0, selected: null}
            },
            skills: {
                acrobatics: {value:-1, selected: true},
                animal: {value: 1, selected: null},
                arcana: {value: 0, selected: null},
                athletics: {value: 5, selected: true},
                deception: {value: 2, selected: null},
                history: {value: 2, selected: null},
                insight: {value: 1, selected: null},
                intimidation: {value: 2, selected: null},
                investigation: {value: 0, selected: null},
                medicine: {value: 1, selected: null},
                nature: {value: 0, selected: null},
                perception: {value: 3, selected: null},
                performance: {value: 2, selected: null},
                persuasion: {value: 4, selected: null},
                religion: {value: 0, selected: null},
                slight: {value: -1, selected: null},
                stealth: {value: -2, selected: null},
                survival: {value: 1, selected: null}
            },
            wisdom: 13,
            other: {
                proficiencies: ['All armor','shields', 'simple weapons', 'martial weapons'],
                languages: ['Common', 'Draconic']
            }
        },
        stats: {
            armor: 18,
            initiative: 0,
            speed: 30,
            hp: 65,
            maxHp: 88,
            tempHp: 0,
            hitDice: 2,
            hitTotal: 8,
            deathSuccess: {
                current: 1,
                max: 3
            },
            deathFalures: {
                current: 0,
                max: 3
            }
        },
        attacks: [
            {
                name: "Sword",
                attBonus: 5,
                damage: "1d8 + 3",
                type: "slashing",
                notes: ""
            },
            {
                name: "Throwing Knife",
                attBonus: 5,
                damage: "1d6 + 3",
                type: "piercing",
                notes: "You can throw a knife 30 feet, or up to 120 feet with disadvantage on the attack roll."
            }
        ],
        equipment: {
            items: [
                {
                    name: "Chain mail",
                    quantity: 1,
                    weight: 35,
                    notes: "While wearing this armor, you have a disadvantage on Dex (stealth) checks."
                },{
                    name: "Sword",
                    quantity: 1,
                    weight: 4,
                    notes: null
                },{
                    name: "Throwing Knife",
                    quantity: 6,
                    weight: 1,
                    notes: ""
                },{
                    name: "Backpack",
                    quantity: 1,
                    weight: 1,
                    notes: "Yes!"
                },{
                    name: "Blanket",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "Tinderbox",
                    quantity: 1,
                    weight: 0.1,
                    notes: ""
                },{
                    name: "Days of rations",
                    quantity: 2,
                    weight: 1,
                    notes: ""
                },{
                    name: "waterskin",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "set of fine clothes",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "signet ring",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                },{
                    name: "scroll of pedigree",
                    quantity: 1,
                    weight: 1,
                    notes: ""
                }
            ],
            currency: {
                cp: null,
                sp: null,
                ep: null,
                gp: 24,
                pp: null
            }
        },
        traits: {
            personality: [
                {
                    type: "Danger",
                    description: "Danger exists"
                },{
                    type: "Torment",
                    description: "My curse etc"
                }
            ],
            ideals: [
                {
                    type: "Monster",
                    description: "Monster will destroy everyhing like the monster i am (evil)"
                }
            ],
            bonds: [
                {
                    type: "Rival",
                    description: "a monster of habbit"
                },{
                    type: "Tiamat",
                    description: "The evil dragon goddess"
                }
            ],
            flaws: [
                {
                    type: "Rituals",
                    description: "a monster of habbit"
                }
            ]
        }
    }
];