import React from 'react';
import ReactDOM from 'react-dom';
import {characters} from './data';
import {Info} from './components/info';
import {Attributes} from './components/attributes';
import Equipment from './components/equipment';
import Stats from './components/stats';
import Traits from './components/traits';
import Header from './components/header';
import './app.scss';

class CharacterSheet extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            characters: characters.filter( (e) => e.info.name.length > 0),
            character: characters[0]
        }
    }

    characterClick(i) {
        this.setState({
            character: characters[i]
        });
    }

    render() {
        const characters = this.state.characters;
        const character = this.state.character;
        const charNames = characters.map( (e,i) => {
            return e.info.name;
        });

        return (
            <div className="container bg-dark">
                <Header names={charNames} onClick={(i) => this.characterClick(i)}/>
                <h1>{character.info.name}</h1>
                <div className="row">
                    <div className="col-12 mb-3">
                        <h2 className='h3'>Character info</h2>
                        <Info character={character.info} />
                    </div>
                    <div className="col-12 col-md-6 mb-3">
                        <Attributes attributes={character.attributes} />
                    </div>
                    <div className="col-12 col-md-6 mb-3">
                        <Stats stats={character.stats} />
                        <Equipment inventory={character.equipment} />
                    </div>
                    <div className="col-12 mb-3">
                        <Traits traits={character.traits} />
                    </div>
                </div>
            </div>
        );
    }
}



ReactDOM.render(
    <CharacterSheet />,
    document.getElementById('root')
);